﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace DeFoe
{
    class Helper
    {
        [DllImport("user32.dll", SetLastError = true)]
        private static extern short GetKeyState(ushort virtualKeyCode);

        internal static bool isKeyDown(Keys keyCode)
        {
            short keyState = GetKeyState((ushort)keyCode);
            return keyState < 0;
        }

        internal static Keys[] keysArrayFromString(string keysStr)
        {
            Keys[] none = new Keys[] { Keys.None };

            if (string.IsNullOrWhiteSpace(keysStr)) return none;

            if (keysStr.Contains("+"))
            {
                string[] keysStrArr = keysStr.Split('+');
                var listKeys = new List<Keys>();

                for (int i = 0; i < keysStrArr.Length; i++)
                {
                    Keys kKey;

                    if (Enum.TryParse(keysStrArr[i], out kKey))
                        listKeys.Add(kKey);
                    else
                        return none;
                }

                return listKeys.ToArray();
            }
            else
            {
                Keys kKey;

                if (Enum.TryParse(keysStr, out kKey))
                    return new Keys[] { kKey };
                else
                    return none;
            }
        }

        internal static bool allKeysPressed(Keys[] keysNeedToBePressed, Keys keyJustPressed)
        {
            for (int i = 0; i < keysNeedToBePressed.Length; i++)
            {
                if (keysNeedToBePressed[i] != keyJustPressed && !isKeyDown(keysNeedToBePressed[i])) return false;
            }

            return true;
        }
    }
}
