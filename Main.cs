﻿using GTA;
using GTA.Native;
using System;
using System.Linq;
using System.Windows.Forms;

namespace DeFoe
{
    internal static class Extensions
    {
        internal static int clamp(this int limitedInt, int min, int max)
        {
            if (limitedInt < min) return min;
            if (limitedInt > max) return max;

            return limitedInt;
        }
    }

    public class DeFoe : Script
    {
        readonly int[] copTypes = { 6, 7 };

        int interval;
        float getPedsRadius, killNearbyPedsRadius;
        bool enabled, actUponAnyEnemy, killCops, disableInMissions;
        Keys[] killNearbyPedsKeys;

        public DeFoe()
        {
            enabled = Settings.GetValue("Settings", "Automatic_Kill_Enabled", true);

            if (enabled)
            {
                interval = Settings.GetValue("Settings", "Interval", 100).clamp(1, 1000);
                getPedsRadius = Settings.GetValue<int>("Settings", "Get_Peds_Radius", 100).clamp(1, 1000);
                actUponAnyEnemy = Settings.GetValue("Settings", "Act_Upon_Any_Enemy", false);
                killCops = Settings.GetValue("Settings", "Kill_Cops", false);
            }

            disableInMissions = Settings.GetValue("Settings", "Disable_In_Missions", true);
            killNearbyPedsKeys = Helper.keysArrayFromString(Settings.GetValue("Kill_All_Nearby_Peds", "Key", "K"));
            killNearbyPedsRadius = Settings.GetValue<int>("Kill_All_Nearby_Peds", "Radius", 100);

            if (enabled)
            {
                Interval += interval;
                Tick += DeFoe_Tick;
            }

            if (killNearbyPedsKeys != new Keys[] { Keys.None }) KeyDown += DeFoe_KeyDown;
        }

        private void killNearbyPeds(float radius, bool killAll)
        {
            if (disableInMissions && Game.MissionFlag) return;

            Player plr = Game.Player;
            if (plr == null || !plr.Exists()) return;

            Ped plrPed = plr.Character;
            if (plrPed == null || !plrPed.Exists() || !plrPed.IsAlive) return;

            Ped[] nearbyPeds = World.GetNearbyPeds(plrPed, radius);
            if (nearbyPeds == null || nearbyPeds.Length == 0) return;

            if (killAll) UI.ShowSubtitle("Killing nearby peds");
            
            for (int i = 0; i < nearbyPeds.Length; i++)
            {
                Yield();

                if (!killAll)
                {
                    if (nearbyPeds[i] == null || !nearbyPeds[i].Exists() || !nearbyPeds[i].IsAlive) continue;

                    //stop if is companion of player, is in same relationship group as player, or is cop
                    if (nearbyPeds[i].GetRelationshipWithPed(plrPed) == Relationship.Companion || nearbyPeds[i].RelationshipGroup == plrPed.RelationshipGroup || (!killCops && copTypes.Contains(Function.Call<int>(Hash.GET_PED_TYPE, nearbyPeds[i]))))
                        continue;

                    if (!nearbyPeds[i].IsInCombat && !nearbyPeds[i].IsInMeleeCombat) continue;

                    //stop if not in combat against me and set to only act upon my actors
                    if (!actUponAnyEnemy && !nearbyPeds[i].IsInCombatAgainst(plrPed) && nearbyPeds[i].GetMeleeTarget() != plrPed) continue;
                }

                nearbyPeds[i].Weapons.Select(WeaponHash.Unarmed, true);

                nearbyPeds[i].Kill();
            }
        }

        private void DeFoe_KeyDown(object sender, KeyEventArgs e)
        {
            if (Helper.allKeysPressed(killNearbyPedsKeys, e.KeyCode))
                killNearbyPeds(killNearbyPedsRadius, true);
        }

        private void DeFoe_Tick(object sender, EventArgs e)
        {
            killNearbyPeds(getPedsRadius, false);
        }
    }
}